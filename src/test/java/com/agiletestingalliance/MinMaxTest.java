package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class MinMaxTest {

          @Test
          public void testMinMaxAttrib2() throws Exception {
                     int result = new MinMax().calcMinMax(2,4);
                     assertEquals("Attribute 2 is greater",result, 4);
          }

          @Test
          public void testMinMaxAttrib1() throws Exception {
                     int result = new MinMax().calcMinMax(4,2);
                     assertEquals("Attribute 1 is greater",result, 4);
          }
}
